package animals;
import java.util.*;

public class Animal{
	public ArrayList<Moveable> moves;
	public String name;
	public ArrayList<String> movement = new ArrayList<>();

	public Animal(String name, ArrayList<Moveable> moves){
		this.name = name;
		this.moves = moves;
	}

	public String getName() {
		return name;
	}

	public String moveName(){
		StringJoiner result = new StringJoiner(", ");

		for(Moveable move : moves){
			result.add(move.getMoveName());
		}
		return result.toString();
	}

	public String doingRandom(){
		for(Moveable move : moves){
			movement.add(move.doing());
		}
		Random random = new Random(); 
        return movement.get(random.nextInt(movement.size())); 
	}
}