package animals;
import java.util.*;

public class Program{
	public static void main(String [] args){
		ArrayList<Moveable> dogMoves = new ArrayList<Moveable>();
		dogMoves.add(new Walk());
		dogMoves.add(new Swim());
		dogMoves.add(new Run());

		Dog theDog = new Dog("Fido", dogMoves);
		System.out.println(theDog.info());

		ArrayList<Moveable> tigerMoves = new ArrayList<Moveable>();
		tigerMoves.add(new Walk());
		tigerMoves.add(new Climb());
		tigerMoves.add(new Run());

		Tiger theTiger = new Tiger("Furball", tigerMoves);
		System.out.println();
		System.out.println(theTiger.info());

		ArrayList<Moveable> monkeyMoves = new ArrayList<Moveable>();
		monkeyMoves.add(new Walk());
		monkeyMoves.add(new Swim());
		monkeyMoves.add(new Run());
		monkeyMoves.add(new Climb());

		Monkey theMonkey = new Monkey("Julius", monkeyMoves);
		System.out.println();
		System.out.println(theMonkey.info());

		ArrayList<Moveable> birdMoves = new ArrayList<Moveable>();
		System.out.println();
		birdMoves.add(new Walk());
		birdMoves.add(new Fly());

		Parrot theParrot = new Parrot("Polly", birdMoves);
		System.out.println(theParrot.info());
		
	}
}